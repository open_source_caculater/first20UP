package com.HYC.websocket.handler;

import ch.qos.logback.core.db.dialect.SybaseSqlAnywhereDialect;

import com.HYC.room.rootHandler;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.*;

import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

 
public class MessageHandler implements WebSocketHandler {
	//连接队列
	private static final List<WebSocketSession> sessions = new ArrayList<>();

   private rootHandler roomHandler = new rootHandler();
//打错字了我靠。。不过这名字好像也不错 用吧
  
	@Override
	public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {

		//TODO 完善心跳检测方案 此处只是测试
		//TODO 进度：客户端主动发ping的话不完善（如果客户端收不到回复，如何处理），服务端主动发ping的还没做
//		this.sendPingMessage(webSocketSession);

		//加入连接队列
		sessions.add(webSocketSession);
		//		System.out.println(webSocketSession);

		System.out.println("刚加入"+webSocketSession);
		webSocketSession.sendMessage(new TextMessage("你与服务器连接成功了！你的sessionID为【" + webSocketSession.getId() + "】"));

		StringBuilder sessionIds = new StringBuilder("");
		for (WebSocketSession session : sessions) {
			session.sendMessage(new TextMessage("用户" + webSocketSession.getId() + "已加入聊天室"));
			sessionIds.append(" " + session.getId() + " ");
		}

		System.out.println("一个客户端连接上了服务器！webSocketSessionId为【" + webSocketSession.getId() + "】, "
				+ "当前服务器session队列中有:【" + sessionIds + "】");

		webSocketSession.sendMessage(new TextMessage("当前聊天室有id为【" + sessionIds + "】的用户"));



	}

	@Override
	public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
		//		Record record = new Gson().fromJson(webSocketMessage.getPayload().toString(), Record.class);

	roomHandler.receiveMessage(webSocketSession, webSocketMessage.getPayload() + "");	 //收到7 创建房间 收到8*  加入房间
 
		 


	}

	@Override
	public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {
		System.out.println("WebsocketSessionId为【 " + webSocketSession.getId() + "】的连接出错！");


	}

	@Override
	public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {

	 
		//将该连接从session队列中移除
		sessions.remove(webSocketSession);
   roomHandler.error(webSocketSession);
	}

	@Override
	public boolean supportsPartialMessages() {
		return false;
	}




	/**
	 * 广播给所有客户端
	 * @param messaage
	 * @throws Exception
	 */
	private void sendToAll(String messaage) throws Exception {
		for (WebSocketSession session : sessions) {
			session.sendMessage(new TextMessage("" + messaage));
		}
	}

	/**
	 * 向客户端发送一个ping/pong信息
	 * @param webSocketSession
	 * @throws Exception
	 */
	private void sendPingMessage(WebSocketSession webSocketSession) throws Exception {
		byte[] bs = new byte[1];
		bs[0] = 'i';//TODO Why is 'i' ?
		ByteBuffer byteBuffer = ByteBuffer.wrap(bs);
		PingMessage pingMessage = new PingMessage(byteBuffer);
		webSocketSession.sendMessage(pingMessage);
		System.out.println("已发送一个ping包：【" + pingMessage.toString() + "】");
	}



}

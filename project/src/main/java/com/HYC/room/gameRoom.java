package com.HYC.room;

import java.io.IOException;
 
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

 

public class gameRoom {

	private String gameNumber;// 房间号
	private int roomOnline = 0;
	private WebSocketSession playerSession;
	private WebSocketSession dealerSession;
	private int hostRight = 0;
	private int guestRight = 0;
	private int hostready = 0;
	private int guestreplay = 0;
	private int hostreplay = 0;
	private int guestready = 0;
	private int waitSessionNumber = 0;

	public void sethostRight(String number) {
		this.hostRight = Integer.parseInt(number);
		this.hostready++;
	}
	public void setguestRight(String number) {
		this.guestRight = Integer.parseInt(number);
		this.guestready++;
	}
	public void setGameNumber(String number, WebSocketSession session) throws IOException, InterruptedException {
		this.gameNumber = number;
		this.roomOnline = 1;
		dealerSession = session;
		System.out.println("这里应该传给客户端房间号了"+gameNumber);
		System.out.println(dealerSession);
		System.out.println("a" + gameNumber+"");
		playerToDealer("a" + gameNumber+"");
	}

	public void joinRoom(WebSocketSession session) throws IOException, InterruptedException {
		// 第二个人加入房间，可以开始游戏
		if(dealerSession == session)
		{
			dealerSession.sendMessage(new TextMessage("p把你的房间号发给朋友，一起来PK吧"));
		}
		else
		{
			if(playerSession == session || dealerSession == session) //already joined
			{
				playerSession.sendMessage(new TextMessage("a" + gameNumber+""));
				broadCast("p房间人数2人，可以开始游戏");
			}else
			{
				if(roomOnline >= 2 && !(playerSession == session || dealerSession == session))//the third man
				{
					session.sendMessage(new TextMessage("p这个房间已经人满啦"));
				}
				else {
					roomOnline++;
					playerSession = session;
					broadCast("p房间人数2人，可以开始游戏");
					playerSession.sendMessage(new TextMessage("a" + gameNumber+""));
					dealerSession.sendMessage(new TextMessage("r"));//给房主 可以开始了
					playerSession.sendMessage(new TextMessage("r"));//给客官 可以开始了
				}
				
			}
		}
		
	}

	public int getRoomOnline() {// 返回房间人数
		return roomOnline;
	}

	private void broadCast(String message) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		// 房间消息广播
		Thread.sleep(100);
		playerSession.sendMessage(new TextMessage(message));
		dealerSession.sendMessage(new TextMessage(message));
	}

	private void beginGame() throws IOException, InterruptedException {


	}

	private void playerToDealer(String message) throws IOException, InterruptedException {
		// 发送消息给房主
		Thread.sleep(100);
		dealerSession.sendMessage(new TextMessage(message));
	}

	private void dealerToPlayer(String message) throws IOException, InterruptedException {
		// 发送信息给玩家
		Thread.sleep(100);
		playerSession.sendMessage(new TextMessage(message));
	}

	public synchronized void receive(WebSocketSession session, String request) throws IOException, InterruptedException {
		if (request.equals("9")) {// 开始
			 if(session == dealerSession &&hostreplay == 0 ) 
			 {
				 if(guestreplay != 1)
				 {playerSession.sendMessage(new TextMessage("k对方准备好了，你点击准备即可开始游戏"));
				 dealerSession.sendMessage(new TextMessage("k等待对方准备"));
				 }
				 hostreplay=1;
				 
				 
			 }
			 if(session == playerSession &&guestreplay == 0 ) 
			 {
				 if(hostreplay != 1)
				 {dealerSession.sendMessage(new TextMessage("k对方准备好了，你点击准备即可开始游戏"));
				 playerSession.sendMessage(new TextMessage("k等待对方准备"));
				 }
				 guestreplay=1;
				 
			 }
			 if((hostreplay == 1 && guestreplay == 1) || (hostreplay == 1 && guestreplay == 1))
			 {
				 hostreplay = 0;
				 guestreplay =0;
				 guestready = 0;
				 hostready = 0;
				 game g1 = new game();
				 
				 broadCast(g1.newPro());
			 }
			 
         } 
		if (request.equals("4")) {// 重玩
			 if(session == dealerSession &&hostreplay == 0 ) 
			 {
				 if(guestreplay != 1)
				 {playerSession.sendMessage(new TextMessage("k对方要和你重赛，点击重玩即可重赛"));
				 dealerSession.sendMessage(new TextMessage("k等待对方接受重赛请求"));
				 }
				 hostreplay=1;
				 
				 
			 }
			 if(session == playerSession &&guestreplay == 0 ) 
			 {
				 if(hostreplay != 1)
				 {dealerSession.sendMessage(new TextMessage("k对方要和你重赛，点击重玩即可重赛"));
				 playerSession.sendMessage(new TextMessage("k等待对方接受重赛请求"));
				 }
				 guestreplay=1;
				 
			 }
			 if((hostreplay == 1 && guestreplay == 1) || (hostreplay == 1 && guestreplay == 1))
			 {
				 hostreplay = 0;
				 guestreplay =0;
				 guestready = 0;
				 hostready = 0;
                game g1 = new game();
				 broadCast(g1.newPro());
			 }
        } 
		
		if (request.substring(0,1).equals("z")) {// 提交做对了几个题
		 System.out.println("HERE is发送状态");
			 if(session == dealerSession && hostready == 0 &&guestready == 0)//房主完成了 只有一个人完成
			 {
				 playerSession.sendMessage(new TextMessage("k房主已经做完了,加油哟"));
				 dealerSession.sendMessage(new TextMessage("k等待对方做完"));
				 sethostRight(request.substring(1));//设置房主的正确题目数量
				 hostready=1;//ready state add
			 }
			 if(session == dealerSession && hostready == 0 &&  guestready == 1 )//房主完成了 全都完成了
			 {
				 sethostRight(request.substring(1));//设置房主的正确题目数量
				 if(hostRight>guestRight)
				 {
					 playerSession.sendMessage(new TextMessage("ks你输了,做对了"+guestRight+"道题目，对方做对了"+hostRight+"道题目"));
					 dealerSession.sendMessage(new TextMessage("ks你赢了,做对了"+hostRight+"道题目，对方做对了"+guestRight+"道题目"));
				 }
				 if(hostRight==guestRight)
				 {
					 broadCast("ks势均力敌 GGEZ");
				 }
				 if(hostRight<guestRight)
				 {
					 playerSession.sendMessage(new TextMessage("ks你赢了,做对了"+guestRight+"道题目，对方做对了"+hostRight+"道题目"));
					 dealerSession.sendMessage(new TextMessage("ks你输了,做对了"+hostRight+"道题目，对方做对了"+guestRight+"道题目"));
				 }
					 
			 }
			 if(session == playerSession && hostready == 0 && guestready ==0 )//成员完成了 完成了一个人
			 {
				 dealerSession.sendMessage(new TextMessage("k你的对手已经完成了，加油"));
				 playerSession.sendMessage(new TextMessage("k等待你的对手完成题目"));
				 setguestRight(request.substring(1));//设置房主的正确题目数量
				 guestready =1;
			 }	
			 if(session == playerSession && hostready == 1&& guestready == 0)//成员完成了 完成了2个人
			 {
				 setguestRight(request.substring(1));//设置房主的正确题目数量
				 if(hostRight>guestRight)
				 {
					 playerSession.sendMessage(new TextMessage("ks你输了,做对了"+guestRight+"道题目，对方做对了"+hostRight+"道题目"));
					 dealerSession.sendMessage(new TextMessage("ks你赢了,做对了"+hostRight+"道题目，对方做对了"+guestRight+"道题目"));
				 }
				 if(hostRight==guestRight)
				 {
					 broadCast("ks势均力敌 GGEZ");
				 }
				 if(hostRight<guestRight)
				 {
					 playerSession.sendMessage(new TextMessage("ks你赢了,做对了"+guestRight+"道题目，对方做对了"+hostRight+"道题目"));
					 dealerSession.sendMessage(new TextMessage("ks你输了,做对了"+hostRight+"道题目，对方做对了"+guestRight+"道题目"));
				 }
				 guestready = 1;
			 }	
			 
			 
			 
			 
         }
		
	
		
	}


	public WebSocketSession getLivingSession(WebSocketSession session) {
		// TODO Auto-generated method stub
		//获得还在房间的session
		if (playerSession != session)
			return playerSession;
		return dealerSession;

	}

	public void removeSession(WebSocketSession session) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		//移除异常的session
		roomOnline--;
		hostready = 0;
		guestready = 0;
		
		waitSessionNumber = 0;
		if (session == dealerSession)
			dealerSession = playerSession;
		playerSession = null;
		playerToDealer("b对方已经退出房间");//中途异常退出模块
	}
	
}

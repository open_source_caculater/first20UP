package com.HYC.room;

import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

 
@Service
public class rootHandler {
	private HashMap<String, gameRoom> NumberIndexRoom = new HashMap<String, gameRoom>();
	private HashMap<WebSocketSession, String> sessionNumber = new HashMap<WebSocketSession, String>();
	 

	public void creatRoom(WebSocketSession session) throws IOException, InterruptedException {
		// 创建房间
		int number = creatRoomNumber();
		gameRoom gameRoom = new gameRoom();
		System.out.println(gameRoom);
		gameRoom.setGameNumber(number + "", session);
	NumberIndexRoom.put(number + "", gameRoom);
		sessionNumber.put(session, number + "");
	}

	private int creatRoomNumber() {
		// 创建唯一的房间号
		Random random = new Random();
		int nextInt;
		while (true) {
			nextInt = random.nextInt(900000) + 100000;
			if (NumberIndexRoom.get(nextInt) == null) {
				return nextInt;
			}
		}
	}

	public void receiveMessage(WebSocketSession session, String request) throws 
	IOException, InterruptedException {
	 
		// 信息接收，判断是对房间操作还是对游戏对局操作
	 
		 //收到7 创建房间 收到8  加入房间
		if (request.equals("7")) {// 创建房间请求
			 String number = sessionNumber.get(session); //根据SESSION找房号 没有找到NUMBER为NULL 允许创建房间
			        
			if(number == null)
			{
				creatRoom(session);  //HASH了SESSION和房间ID  房间ID和GAME 还把这个SESSION设置成了房主
				System.out.println("成功创建了房间"+number);
			}else {
				session.sendMessage(new TextMessage("p你已经在房间中或者加入了一个房间了哦，"));
			}
			System.out.println("这里是创建房间");
			
			System.out.println("这里是创建房间结束");
 
		} 
		if (request.substring(0,1).equals("8")) {// 加入房间 后面跟的是房间号
			System.out.print("这里是加入房间");
			String  number = sessionNumber.get(session); //根据SESSION找房号
			if(number == null)
			{
				joinRoom(session, request.substring(1));
			}else {
				session.sendMessage(new TextMessage("p你已经在房间中或者加入了一个房间了哦，"));
			}
			
         }
		if (request.equals("9")) {// 开始
			String  number = sessionNumber.get(session); //根据SESSION找房号
			gameRoom gameRoom = NumberIndexRoom.get(number);//根据房号找gameroom
			System.out.print(number);
			 gameRoom.receive(session,request);
         } 
		if (request.substring(0,1).equals("z")) {// 提交做对了几个题
			System.out.print("这里是提交做对了几个题");
			String  number = sessionNumber.get(session); //根据SESSION找房号
			gameRoom gameRoom = NumberIndexRoom.get(number);
			gameRoom.receive(session,request); 
			 
         }
		if (request.equals("4")) {// 重玩
			System.out.print("这里是重玩");
			String  number = sessionNumber.get(session); //根据SESSION找房号
			gameRoom gameRoom = NumberIndexRoom.get(number);
			gameRoom.receive(session,request); 
			 
         }
		
		
		
	}

	private void joinRoom(WebSocketSession session, String number) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		// 加入房间
		gameRoom gameRoom = NumberIndexRoom.get(number);//根据房间号找到游戏房间
		System.out.println(gameRoom + "房间");
		if (gameRoom == null) {
			session.sendMessage(new TextMessage("p房间不存在"));
		} else {
			
				gameRoom.joinRoom(session);
				sessionNumber.put(session, number);
				
				System.out.print("顺利加入了房间");
			
		}
	}

	public void error(WebSocketSession session) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		// session异常
		String number = sessionNumber.get(session);
		if (number == null)// 还未加入房间，不用处理异常
			return;
		gameRoom gameRoom = NumberIndexRoom.get(number);
		if (gameRoom.getRoomOnline() == 1) {
			// 如果房间只有一个人，直接删除房间
			NumberIndexRoom.remove(number);
		} else {
			// 房间两个人，移除异常的session
			gameRoom.removeSession(session);
		}
		sessionNumber.remove(session);// session的房间号移除
	}
}

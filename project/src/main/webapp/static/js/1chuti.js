
var array = new Array();
array[0] = ["", "+", "", "", "", ""];
var index = 0, now = 0; // 最后一个题目的下标，当前显示的题目的下标
var bgCounter = 0;
var backgrounds = [
    ["../static/images/1.jpg", "first-slide", "First slide"],
    ["../static/images/2.jpg", "second-slide", "Second slide"],
    ["../static/images/3.jpg", "third-slide", "Third slide"]
];

$(document).ready(function () {
    // 添加输入框
    function addElement() {
        var sign2 = `<select id='sign2' class='form-control' 
                             style='width:38px;padding-top: 2px;padding-right: 2px;padding-left: 2px;padding-bottom: 2px;' 
                             data-live-search='true'>
                        <option value='+'>＋</option>
                        <option value='-'>－</option>
                    </select>`;
        var third = `<input type="text" name="third" class="form-control"
                             style="width:80px;" placeholder="third" id="third" onkeyup="(this.v=function(){this.value=this.value.replace(/[^\\d]/g,'');}).call(this)"
                             onblur="this.v()" 
                             onchange="$('#err').remove()"/>`;
        console.log("append");
        if ($("#sign2").length <= 0) {
            $("input[name='second']").after(sign2 + third);         // 追加新元素
        }
    }

    // 移除多余的输入框
    function removeElement() {
        console.log("remove");
        if ($("#sign2").length > 0) {
            $("#sign2").remove();
            $("input[name='third']").remove();
        }
    }

    // 切换背景
    function changeBackground() {
        bgCounter = (bgCounter + 1) % backgrounds.length;
        $('#background').attr('src', backgrounds[bgCounter][0]); // 改变背景图
        $('#background').attr('class', backgrounds[bgCounter][1]);
        $('#background').attr('alt', backgrounds[bgCounter][2]);

        // setTimeout(changeBackground, 10000);
    }

    // 获取输入框空格个数
    function spaceNum(choose, first, sign1, second, sign2, third, result) {
        var count = 0;
        if (first == "") {
            count++;
        }
        if (second == "") {
            count++;
        }
        if (result == "") {
            count++;
        }
        if (choose == "3" && third == "") {
            count++;
        }
        return count;
    }

    // 算式是否合格，不合格则提示错误信息
    function isLegal(choose, first, sign1, second, sign2, third, result, formula) {
        var outcome = 0;

        if (choose == "2") {
            if (first == "") {
                formula[0] = "?";
                second = parseInt(second);
                result = parseInt(result);
                if (sign1 == "+") {
                    outcome = result - second;
                } else if (sign1 == "-") {
                    outcome = result + second;
                }
            } else if (second == "") {
                formula[2] = "?";
                first = parseInt(first);
                result = parseInt(result);
                if (sign1 == "+") {
                    outcome = result - first;
                } else if (sign1 == "-") {
                    outcome = first - result;
                }
            } else if (result == "") {
                formula[5] = "?"
                first = parseInt(first);
                second = parseInt(second);
                // alert(second);
                if (sign1 == "+") {
                    outcome = first + second;
                } else if (sign1 == "-") {
                    outcome = first - second;
                }
                // alert("outcome=" + outcome);
            }
        } else if (choose == "3") {
            console.log(formula);

            if (first == "") {
                formula[0] = "?";
                second = parseInt(second);
                third = parseInt(third);
                result = parseInt(result);

                if (sign1 == "+") {
                    outcome = result - second;
                } else if (sign1 == "-") {
                    outcome = result + second;
                }

                if (sign2 == "+") {
                    outcome = outcome - third;
                } else if (sign2 == "-") {
                    outcome = outcome + third;
                }

            } else if (second == "") {
                formula[2] = "?";
                first = parseInt(first);
                third = parseInt(third);
                result = parseInt(result);

                if (sign1 == "+") {
                    outcome = result - first;
                } else if (sign1 == "-") {
                    outcome = first - result;
                }

                if (sign2 == "+") {
                    outcome = outcome - third;
                } else if (sign2 == "-") {
                    outcome = outcome + third;
                }

            } else if (third == "") {
                formula[4] = "?";
                first = parseInt(first);
                second = parseInt(second);
                result = parseInt(result);

                if (sign1 == "+") {
                    outcome = result - first;
                } else if (sign1 == "-") {
                    outcome = first - result;
                }

                if (sign2 == "+") {
                    outcome = outcome - second;
                } else if (sign2 == "-") {
                    outcome = outcome + second;
                }

            } else if (result == "") {
                formula[5] = "?";
                first = parseInt(first);
                second = parseInt(second);
                third = parseInt(third);
                if (sign1 == "+") {
                    outcome = first + second;
                } else if (sign1 == "-") {
                    outcome = first - second;
                }
                if (sign2 == "+") {
                    outcome = outcome + third;
                } else if (sign2 == "-") {
                    outcome = outcome - third;
                }
            }
        }

        // alert(first+sign1+second+"="+outcome)
        if (outcome == null || outcome < 0 || outcome > 20) {
            var err = `<div class="form-control col-xs-2 col-md-4" id="err"
                                style="background-color:rgb(245, 198, 203);height:38px;text-align:center; color:rgb(114, 28, 36)">
                                Error:数据超出运算范围！</div>`;

            $('#result').after(err);
            return false;
        } else {
            array[index] = formula;
            index++; // 数组下标+1
            array[index] = new Array(6); // 创建新数组，存题目
            array[index] = ["", "+", "", "", "", ""];
            now = index;
            
            console.log(array);
            return true;
        }
    }
    
    // 清空输入框
    function clearInput() {
        $('#choose').val("2");
        $('#first').val("");
        $('#sign1').val("+");
        $('#second').val("");
        $('#result').val("");
    }

    // 显示题目
    function show() {
        $('#first').val(array[now][0]);
        $('#sign1').val(array[now][1]);
        $('#second').val(array[now][2]);
        $('#result').val(array[now][5]);

        if (array[now][3] != "") {
            addElement();
            $("#choose").val("3");
            $('#sign2').val(array[now][3]);
            $('#third').val(array[now][4]);

        } else {
            removeElement();
            $("#choose").val("2");
        }
        changeBackground();
    }

    // 选择出题 数字个数
    $("#choose").change(function () {
        var opt = $(this).children('option:selected').val();

        $("#err").remove();

        if (opt == "3") {
            addElement();
        } else if (opt == "2") {
            removeElement();
        }
    });

    // 前一题：显示
    $("#preItem").click(function () {
        if (now > 0) { // 还有题目可以显示
            console.log("pre show: " + now);
            now--;
            show();
        }
    });

    // 下一题：可能是出题， 可能是显示
    $("#nextItem").click(function () {

        console.log("next show: now=" + now + "    index=" + index);

        if ($("#err").length > 0) { // 不存在错误提示信息 刷新题目输入格式
            $("#err").remove();
        }

        if (now < index) { // 
            now++;
            console.log("array[" + now + "]: " + array[now])
            show();

        } else {

            var choose, first, sign1, second, sign2 = "", third = "", result;
            choose = $("#choose").val();
            first = $('#first').val();
            sign1 = $('#sign1').val();
            second = $('#second').val();
            result = $('#result').val();

            if ($('#sign2').length > 0) {
                sign2 = $('#sign2').val();
                third = $('#third').val();
            }

            var count = spaceNum(choose, first, sign1, second, sign2, third, result);
            if (count == 1) { // 刚好空一格
                // 初始化
                var formula = [first, sign1, second, sign2, third, result];

                if (isLegal(choose, first, sign1, second, sign2, third, result, formula)){
                    removeElement();
                    changeBackground();
                }

            } else if (count == 0) {

                var err = `<div class="form-control col-xs-2 col-md-3" id="err"
                                    style="background-color:rgb(245, 198, 203);height:38px;text-align:center; color:rgb(114, 28, 36)">
                                    Error:无空以填结果！</div>`;
                $('#result').after(err);


            } else {

                var err = `<div class="form-control col-xs-2 col-md-3" id="err"
                                    style="background-color:rgb(245, 198, 203);height:38px;text-align:center; color:rgb(114, 28, 36)">
                                    Error:题目不完整！</div>`;
                $('#result').after(err);
            }
            clearInput();
        }
    });


    $('#finish').click(function () {
        var choose, first, sign1, second, sign2 = "", third = "", result;
        choose = $("#choose").val();
        first = $('#first').val();
        sign1 = $('#sign1').val();
        second = $('#second').val();
        result = $('#result').val();

        if ($('#sign2').length > 0) {
            sign2 = $('#sign2').val();
            third = $('#third').val();
        }
        if (now == index){ // 当前显示的是输入的最后一题
            var count = spaceNum(choose, first, sign1, second, sign2, third, result);
            if (count == 1) { // 合法算式，存
                var formula = [first, sign1, second, sign2, third, result];
                isLegal(choose, first, sign1, second, sign2, third, result, formula);
            }
        }
        var json=[];
        array.slice(0, array.length - 1).forEach(function(item){
    	    var temp={};
    	    item.forEach(function(value,index){
    	    	console.log('INDEX'+index);
    	    	console.log('value'+value);
    	        temp[index]=value;
    	    });
    	    json.push(temp); //[{},{},{},{},{}]
    	})
    	var str = JSON.stringify(json);  
    	$.cookie('testJSON', str);

       // var str = JSON.stringify(array.slice(0, array.length - 1));
       // $.cookie('testJSON', str);
      //  alert(str);
    });

})

var i;

$("document").ready(function () {
    //弹出浮层，显示题目
var itime;
  //  var array = $.cookie('data');
    // var array = $.session.get('data');
    
    
    console.log("答题界面JSON"+$.cookie('testJSON'));
    
    var list = JSON.parse($.cookie('testJSON'));
    var arr = [];
    for (var i in list) {
    	   var item = [];
    	    for (var j in list[i]) item.push(list[i][j]);
    	    console.log("item"+item)
    	    arr.push(item)
    	}
    var array = [];
    for(var x = 0 ;x < arr.length ;x++)
    	{
    	 array[x]=new Array();        //声明二维数组
         for(var y=0;y<=5;y++){
              array[x][y]=arr[x][y];          //数组初始化为0
         }
    	}
    
    
 
 
    $("#content_ok").show(function () {

        var index =array.length;
        // var array = new Array();
        // array[index] = new Array(6);

        // //测试数据
        // index = 3;
        // // 1 + 2 = ?
        // array[0] = ["1", "+", "2", "", "", "?"];
        // // 2 - 1 + 3 = ?
        // array[1] = ["2", "-", "1", "+", "3", "?"];
        // // ？ + 4 = 5
        // array[2] = ["?", "+", "4", "", "", "5"];
        // // 4 - 3 + ? = 7
        // array[3] = ["4", "-", "3", "+", "?", "7"];

        // 拼接
        var itemsHtml = '';
        var d = 0;
        while (d < array.length) {
            // 第一列tr开头 <tr><th></th><th></th></tr>形式做两列多行
            if (d % 2 == 0) {
                itemsHtml += '<tr>';
            }

            var t = d + 1;
            itemsHtml += '<th id="t' + d + '"> (' + t + ')';

            // 拼接题目内容（1题）
            for (var i = 0; i <= 5; i++) {
                // 跳过为空的数，区分二目与三目加减
                if (array[d][i] !== "") {
                    if (i < 5) {
                        if (array[d][i] == "?") {
                            itemsHtml += '<input type="text">';
                        } else {
                            itemsHtml += array[d][i];
                        }
                    } else { // result之前加等号
                        itemsHtml += '=';
                        if (array[d][i] == "?") {
                            itemsHtml += '<input type="text">';
                        } else {
                            itemsHtml += array[d][i];
                        }
                    }
                }
            }
            // 一题结束，结束一列
            itemsHtml += '</th>';
            // 第二列结束一行
            if (d % 2 != 0) {
                itemsHtml += '</tr>';
            }
            d++;
        }
        $("#xianshi").append(itemsHtml);

        //清除之前的样式，显示浮层
        $("#fullScreen,#floatLayer").remove();
        $("body").append(
            //占据整个屏幕Div
            "<div id='fullScreen'></div>" +
            //浮层区
            "<div id='floatLayer'>" +
            "<button href='#' class='fudong medium blue rounded button'id='zuoti' >直接开始</a>" +

            "<button href='/jiexi.html' class='fudong medium green rounded button'id='jishi' onClick='timedCount()' >计时开始</a>" +

            "</div>"
        );

        //隐藏浮层
        $("#zuoti").click(function () {
            $("#fullScreen,#floatLayer").remove();
            $("#lazy,#jishi123").remove();
            $("span").remove(".timespan");
        });
        $("#jishi").click(function () {
            $("#fullScreen,#floatLayer").remove();
            var starttime = new Date();
            itime = setInterval(function () {
                var nowtime = new Date();
                var time = nowtime - starttime;
                var hour = parseInt(time / 1000 / 60 / 60 % 24);
                var minute = parseInt(time / 1000 / 60 % 60);
                var seconds = parseInt(time / 1000 % 60);
                $('.timespan').html(hour + "小时" + minute + "分钟" + seconds + "秒");
            }, 1000);
        });

        function getDate() {
            var today = new Date();
            var date = today.getFullYear() + "年" + twoDigits(today.getMonth() + 1) + "月" + twoDigits(today.getDate()) + "日 ";
            var week = " 星期" + "日一二三四五六 ".charAt(today.getDay());
            var time = twoDigits(today.getHours()) + ":" + twoDigits(today.getMinutes()) + ":" + twoDigits(today.getSeconds());
            $(".datespan").html(date + week + time);
        }

        function twoDigits(val) {
            if (val < 10) return "0" + val;
            return val;
        }
        $(function () {
            setInterval(getDate, 1000);
        });


        //结束答题，停止计时，自动阅卷，出现统计
        $("#jiaojuan").click(function () {
            // 停止计时
            clearInterval(itime);


            // 以下为自动阅卷代码
            var wtm = 0,
                first = 0,
                second = 0,
                third = 0,
                sign1 = "",
                sign2 = "",
                result = 0,
                right = 0,
                worry = 0;

            // 读取题目内容（从i+1到index+1题）
            for (var i = 0; i < index; i++) {
                // 提取第（i+1）题input中的值
                wtm = $("#t" + i + "").find("input").val();
                console.log("val=" + wtm + "   t=" + (i + 1));
                // 获取题目数据（第1，2个数字与结果）
                first = array[i][0];
                sign1 = array[i][1];
                second = array[i][2];
                sign2 = array[i][3];
                third = array[i][4];
                result = array[i][5];
                console.log("first=" + first + "   second=" + second + "   third=" + third + "   result=" + result);

                // 二目加减的情况（第2个符号位sign2为空）
                if (sign2 == "") {
                    // 加法
                    if (sign1 == "+") {
                        //答题正确
                        if ((first == "?" && wtm == (parseInt(result) - parseInt(second))) || (second == "?" && wtm == parseInt((result) - parseInt(first))) || (result == "?" && wtm == (parseInt(first) + parseInt(second)))) {
                            $("#t" + i + "").css({
                                "color": "blue",
                                "opacity": 1
                            });
                            right++;
                        } else {
                            $("#t" + i + "").css({
                                "color": "red",
                                "opacity": 1
                            });
                            worry++;
                            // 此处可能有解析但我不会写


                        }
                    } else { //减法
                        if ((first == "?" && wtm == (parseInt(result) + parseInt(second))) || (second == "?" && wtm == (parseInt(first) - parseInt((result)))) || (result == "?" && wtm == (parseInt(first) - parseInt(second)))) {
                            $("#t" + i + "").css({
                                "color": "blue",
                                "opacity": 1
                            });
                            right++;
                        } else {
                            $("#t" + i + "").css({
                                "color": "red",
                                "opacity": 1
                            });
                            worry++;
                            //

                        }
                    }
                    // 三目加减的情况，真让人头大
                } else if (sign2) {

                    //第一个符号位为+
                    if (sign1 == "+") {
                        // 第二个符号位为+

                        if (sign2 == "+") {
                            if (
                                (first == "?" && wtm == (parseInt(result) - parseInt(third) - parseInt(second))) ||
                                (second == "?" && wtm == (parseInt(result) - parseInt(third) - parseInt(first))) ||
                                (third == "?" && wtm == (parseInt(result) - parseInt(first) - parseInt(second))) ||
                                (result == "?" && wtm == (parseInt(first) + parseInt(second) + parseInt(third)))) {
                                $("#t" + i + "").css({
                                    "color": "blue",
                                    "opacity": 1
                                });
                                right++;
                            } else {
                                $("#t" + i + "").css({
                                    "color": "red",
                                    "opacity": 1
                                });
                                worry++;
                            }

                        } else { // 第二个符号位为-
                            if (
                                (first == "?" && wtm == (parseInt(result) - parseInt(second) + parseInt(third))) ||
                                (second == "?" && wtm == (parseInt(result) - parseInt(first) + parseInt(third))) ||
                                (third == "?" && wtm == (parseInt(first) + parseInt(second) - parseInt(result))) ||
                                (result == "?" && wtm == (parseInt(first) + parseInt(second) + parseInt(third)))) {
                                $("#t" + i + "").css({
                                    "color": "blue",
                                    "opacity": 1
                                });
                                right++;
                            } else {
                                $("#t" + i + "").css({
                                    "color": "red",
                                    "opacity": 1
                                });
                                worry++;
                            }
                        }
                    } else { // 第一个符号位为-
                        if (sign2 == "+") { // 第二个符号位为+
                            if (
                                (first == "?" && wtm == (parseInt(result) + parseInt(second) - parseInt(third))) ||
                                (second == "?" && wtm == (parseInt(first) + parseInt(third) - parseInt(result))) ||
                                (third == "?" && wtm == (parseInt(result) - parseInt(first) + parseInt(second))) ||
                                (result == "?" && wtm == (parseInt(first) - parseInt(second) + parseInt(third)))) {
                                $("#t" + i + "").css({
                                    "color": "blue",
                                    "opacity": 1
                                });
                                right++;
                            } else {
                                $("#t" + i + "").css({
                                    "color": "red",
                                    "opacity": 1
                                });
                                worry++;
                            }
                        } else { // 第二个符号位为-
                            if (
                                (first == "?" && wtm == (parseInt(result) + parseInt(second) + parseInt(third))) ||
                                (second == "?" && wtm == (parseInt(first) - parseInt(third) - parseInt(result))) ||
                                (third == "?" && wtm == (parseInt(first) - parseInt(second) - parseInt(result))) ||
                                (result == "?" && wtm == (parseInt(first) - parseInt(second) - parseInt(third)))) {
                                $("#t" + i + "").css({
                                    "color": "blue",
                                    "opacity": 1
                                });
                                right++;
                            } else {
                                $("#t" + i + "").css({
                                    "color": "red",
                                    "opacity": 1
                                });
                                worry++;
                            }
                        }
                    }
                }


            }

            // 统计
            $("#tongji").remove();
            $("#jiaojuan    ").after("<center><p id='tongji'>正确：" + right + "                 错误：" + worry + "</p>");
        });
    });
});
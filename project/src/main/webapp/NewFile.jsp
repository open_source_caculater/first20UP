<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--demo的JS  -->
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.0.js">
</script>
 <script type="text/javascript" src="../css/js/jquery-1.6.js"></script>
<script type="text/javascript" src="../css/js/Vegur_700.font.js"></script>
<script type="text/javascript" src="../css/js/Vegur_400.font.js"></script>
<script type="text/javascript" src="../css/js/Vegur_300.font.js"></script>
<script type="text/javascript" src="../css/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="../css/js/tms-0.3.js"></script>
<script type="text/javascript" src="../css/js/tms_presets.js"></script>
<script type="text/javascript" src="../css/js/backgroundPosition.js"></script>
<script type="text/javascript" src="../css/js/atooltip.jquery.js"></script>
<script type="text/javascript" src="../css/js/script.js"></script>
  <!-- <link rel="stylesheet" href="css1/style.css"> -->
  
     <script src="../js/chuti.js"></script>
<script type="text/javascript" src="../js/jquery.beattext.js"></script>
<script type="text/javascript" src="../js/easying.js"></script>
<!--  -->
 
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="../css/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<style>
span, span:after {
    font-weight: 900;
    color: #efedce;
    white-space: nowrap;
    display: inline-block;
    position: relative;
    letter-spacing: .1em;
    padding: .2em 0 .25em 0;
}

span {
    font-size: 1em;
    z-index: 100;
    text-shadow: .08em .08em 0 #9cb8b3;
}

span:after {
    content: attr(data-shadow-text);
    color: rgba(0,0,0,.35);
    text-shadow: none;
    position: absolute;
    left: .0875em;
    top: .0875em;
    z-index: -1;
    -webkit-mask-image: url(https://f.cl.ly/items/1t1C0W3y040g1J172r3h/mask.png);
}

@import url(https://fonts.googleapis.com/css?family=Lato:900);
*, *:before, *:after{
  box-sizing:border-box;
}
.letter{
  display: inline-block;
  font-weight: 900;
  font-size: 2em;
  margin: 0.2em;
  position: relative;
  color: #00B4F1;
  transform-style: preserve-3d;
  perspective: 400;
  z-index: 1;
}
.letter:before, .letter:after{
  position:absolute;
  content: attr(data-letter);
  transform-origin: top left;
  top:0;
  left:0;
}
.letter, .letter:before, .letter:after{
  transition: all 0.3s ease-in-out;
}
.letter:before{
  color: #fff;
  text-shadow: 
    -1px 0px 1px rgba(255,255,255,.8),
    1px 0px 1px rgba(0,0,0,.8);
  z-index: 3;
  transform:
    rotateX(0deg)
    rotateY(-15deg)
    rotateZ(0deg);
}
.letter:after{
  color: rgba(0,0,0,.11);
  z-index:2;
  transform:
    scale(1.08,1)
    rotateX(0deg)
    rotateY(0deg)
    rotateZ(0deg)
    skew(0deg,1deg);
}
.letter:hover:before{
  color: #fafafa;
  transform:
    rotateX(0deg)
    rotateY(-40deg)
    rotateZ(0deg);
}
.letter:hover:after{
  transform:
    scale(1.08,1)
    rotateX(0deg)
    rotateY(40deg)
    rotateZ(0deg)
    skew(0deg,22deg);
}
 
</style>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
     <script>
        $(document).ready(function(){
            $('#con').load('../htmls/main.html');
            $("#a1").click(function(){
            	$("#con").empty();
                $('#con').load('../htmls/main.html');
            }); 
            $("#a2").click(function(){
            	$("#con").empty();
                $('#con').load('../htmls/demo.html');
            });
            $("#a3").click(function(){
                $('#con').load('../htmls/chuti.html');
            });
            $("#a4").click(function(){
                $('#con').load('../htmls/war.html');
            });
        })
    </script>

</head>
<body>
<!-- start header -->
<div class="header_bg" id="nav" style ="width:`100%;
height:30%;">
	
		<div class="header">
			<div class="logo">
				<a href="#">
					<img src="../images/lg.png" alt=""/>
					<h1><span data-shadow-text="HAPPY KIDS">HAPPY KIDS</span></h1>
					
				 </a>
			</div>
			<div class="header_sub">
				<div class="h_menu">
					<ul >
						<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
						<li><a href="#"   id="a1"><span class="letter" data-letter="首页">首页</span></a></li>
						<li><a href="#" id="a2"><span class="letter" data-letter="练习">练习</span></a></li>
						<li><a href="#"  id="a3"><span class="letter" data-letter="出题">出题</span></a></li>
						<li><a href="#"  id="a4"><span class="letter" data-letter="双人对战">双人对战</span></a></li>
					</ul>
					</div>
					<div class="clear"> </div>
			</div>
	<div class="clear"> </div>
	</div>
</div>
<div id="con" style = "width:100%;
height:70%;"></div>	
</body>
</html>